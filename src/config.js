import store from './store'

const items = [
  {
    id: 0,
    name: 'Сумма',
    function: () => { store.dispatch('sum') },
    sendCopyTo: null
  },
  {
    id: 1,
    name: 'Константа',
    function: () => { store.dispatch('setDefaultValue') },
    sendCopyTo: 2
  },
  {
    id: 2,
    name: '',
    function: '',
    sendCopyTo: 1
  }
]

export { items }
